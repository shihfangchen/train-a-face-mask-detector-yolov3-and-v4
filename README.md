# Train a Face Mask Detector YOLOv3 and v4

Train a Face Mask Detector YOLOv3 and v4

v3 demo 4000 epochs : https://youtu.be/jU6q3Iqd_mw (mAP@0.5 = 83%)

v4 demo, early stopping at ~2000 epochs : https://youtu.be/ZpnFvfrNXd8  (mAP@0.5 = 92%)